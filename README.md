ASF Challenge <img src="https://forgemia.inra.fr/uploads/-/system/project/avatar/1735/estherwonderpig-compressed.jpg?width=64" align="right" width = 100px/>
=============

[https://www6.inrae.fr/asfchallenge/](https://www6.inrae.fr/asfchallenge/)

Open modelling challenge on African swine fever epidemics.

Team ASTRE: Vladimir Grosbois, Ferran Jori, Facundo Muñoz, David
Pleydell

Working documents: 

- [Slides presentation of approach](https://umr-astre.pages.mia.inra.fr/asf-challenge/presentation_astre.html)

- [Exploratory data analysis](https://umr-astre.pages.mia.inra.fr/asf-challenge/descriptive_analysis.html)

- [Farm-network model](https://umr-astre.pages.mia.inra.fr/asf-challenge/model_farms_network.html)

- [Simulation model](https://umr-astre.pages.mia.inra.fr/asf-challenge/simulationModel.html)

- [Predictive performance of submission 2](https://umr-astre.pages.mia.inra.fr/asf-challenge/eval_submission2.html)

- [Time-to event modelling and daily exposure](https://umr-astre.pages.mia.inra.fr/asf-challenge/model_farms_survival.html)


<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://forgemia.inra.fr/umr-astre/asf-challenge">ASF Challenge modelling</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://umr-astre.cirad.fr/">Astre Team</a> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p> 
