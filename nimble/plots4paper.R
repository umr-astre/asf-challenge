library(sf)
library(tmap)
library(tmaptools)
library(dplyr)
library(here)
options(width=1000)

baseDir <- here("nimble")
figsDir <- here("nimble/figuresWB")
simDir1 <- here("nimble/model4/newSims_report1/")
simDir2 <- here("nimble/model10/MCMCall/newSims_report2/")
simDir3 <- here("nimble/model12/MCMCall1000/newSims_report3/")

############################################################
## Load R objects required for mapping the simualted data ##
############################################################
load(here("nimble/objectsForMapping.Rdata"))

############################################
## Identify simulation files for report 1 ##
############################################
setwd(here("nimble/model4"))
simFiles_r1s1 <- read.table("report1_scenario1_selectedSims.txt", header=TRUE)[2:4,"fileName"]
simFiles_r1s3 <- read.table("report1_scenario3_selectedSims.txt", header=TRUE)[2:4,"fileName"]
simFiles_r1s4 <- read.table("report1_scenario4_selectedSims.txt", header=TRUE)[2:4,"fileName"]
## Add path to file names
(simFiles_r1s1 <- paste0(simDir1,simFiles_r1s1))
(simFiles_r1s3 <- paste0(simDir1,simFiles_r1s3))
(simFiles_r1s4 <- paste0(simDir1,simFiles_r1s4))

############################################
## Identify simulation files for report 2 ##
############################################
setwd(here("nimble/model10"))
simFiles_r2s1 <- read.table("report2_scenario1_selectedSims.txt", header=TRUE)[2:4,"fileName"]
simFiles_r2s2 <- read.table("report2_scenario2_selectedSims.txt", header=TRUE)[2:4,"fileName"]
simFiles_r2s3 <- read.table("report2_scenario3_selectedSims.txt", header=TRUE)[2:4,"fileName"]
simFiles_r2s4 <- read.table("report2_scenario4_selectedSims.txt", header=TRUE)[2:4,"fileName"]
simFiles_r2s5 <- read.table("report2_scenario5_selectedSims.txt", header=TRUE)[2:4,"fileName"]
## Add path to file names
(simFiles_r2s1 <- paste0(simDir2,simFiles_r2s1))
(simFiles_r2s2 <- paste0(simDir2,simFiles_r2s2))
(simFiles_r2s3 <- paste0(simDir2,simFiles_r2s3))
(simFiles_r2s4 <- paste0(simDir2,simFiles_r2s4))
(simFiles_r2s5 <- paste0(simDir2,simFiles_r2s5))

############################################
## Identify simulation files for report 3 ##
############################################
setwd(here("nimble/model12"))
simFiles_r3s1 <- read.table("report3_scenario1_selectedSims.txt", header=TRUE)[2:4,"fileName"]
simFiles_r3s3 <- read.table("report3_scenario3_selectedSims.txt", header=TRUE)[2:4,"fileName"]
## Add path to file names
(simFiles_r3s1 <- paste0(simDir3,simFiles_r3s1))
(simFiles_r3s3 <- paste0(simDir3,simFiles_r3s3))

############################
## Common mapping objects ##
############################
drake::loadd(mapAreaR3)
ZOOM = FALSE ## TRUE
if (ZOOM==TRUE) {
  mapBase = tm_shape(hexSub_at_D110) + tm_polygons(col = "pForest", lwd=0.2, border.col="grey", palette=get_brewer_pal("Greens", n = 10, plot=FALSE))
} else {
  mapBase = tm_shape(mapAreaR3) + tm_borders(col=rgb(0,0,0,0)) + tm_shape(hexAll) + tm_polygons(col = "pForest", lwd=0.2, border.col="grey", palette=get_brewer_pal("Greens", n = 10, plot=FALSE))
}
mapOverays =
  tm_shape(admin) + tm_borders(lwd=2) +
  tm_shape(fence) + tm_borders(col="darkviolet", lwd=2) +
  tm_shape(huntZone) + tm_borders(col="red", lwd=2)


#######################
## Maps for report 1 ##
#######################
DAY = 80
## Infectious: p025
load(simFiles_r1s1[1])
hexAll$I  = I[which(tVec==DAY),]
mapI_rep1_p025 = mapBase
if(any(hexAll$I > 0)) {
  mapI_rep1_p025 = mapI_rep1_p025 + tm_shape(hexAll %>% filter(hexAll$I > 0)) + tm_polygons(col="I", border.alpha=1)
}
mapI_rep1_p025 = mapI_rep1_p025 + mapOverays + tm_layout(main.title = paste0("Optimistic (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)
## Infectious: median
load(simFiles_r1s1[2])
hexAll$I  = I[which(tVec==DAY),]
mapI_rep1_med = mapBase
if(any(hexAll$I > 0)) {
  mapI_rep1_med = mapI_rep1_med + tm_shape(hexAll %>% filter(hexAll$I > 0)) + tm_polygons(col="I", border.alpha=1)
}
mapI_rep1_med = mapI_rep1_med + mapOverays + tm_layout(main.title = paste0("Average (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)
## Infectious: p975
load(simFiles_r1s1[3])
hexAll$I  = I[which(tVec==DAY),]
mapI_rep1_p975 = mapBase
if(any(hexAll$I > 0)) {
  mapI_rep1_p975 = mapI_rep1_p975 + tm_shape(hexAll %>% filter(hexAll$I > 0)) + tm_polygons(col="I", border.alpha=1)
}
mapI_rep1_p975 = mapI_rep1_p975 + mapOverays + tm_layout(main.title = paste0("Pessimistic (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)

#######################
## Maps for report 2 ##
#######################
DAY = 110
## Infectious: p025
load(simFiles_r2s1[1])
hexAll$I       = I[which(tVec==DAY),]
mapI_rep2_p025 = mapBase
if(any(hexAll$I > 0)) {
  mapI_rep2_p025 = mapI_rep2_p025 + tm_shape(hexAll %>% filter(hexAll$I > 0)) + tm_polygons(col="I", border.alpha=1)
}
mapI_rep2_p025 = mapI_rep2_p025 + mapOverays + tm_layout(main.title = paste0("Optimistic (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)
## Infectious: p025
load(simFiles_r2s1[2])
hexAll$I  = I[which(tVec==DAY),]
mapI_rep2_med = mapBase
if(any(hexAll$I > 0)) {
  mapI_rep2_med = mapI_rep2_med + tm_shape(hexAll %>% filter(hexAll$I > 0)) + tm_polygons(col="I", border.alpha=1)
}
mapI_rep2_med = mapI_rep2_med + mapOverays + tm_layout(main.title = paste0("Average (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)
## Infectious: p025
load(simFiles_r2s1[3])
hexAll$I  = I[which(tVec==DAY),]
mapI_rep2_p975 = mapBase
if(any(hexAll$I > 0)) {
  mapI_rep2_p975 = mapI_rep2_p975 + tm_shape(hexAll %>% filter(hexAll$I > 0)) + tm_polygons(col="I", border.alpha=1)
}
mapI_rep2_p975 = mapI_rep2_p975 + mapOverays + tm_layout(main.title = paste0("Pessimistic (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)

#######################
## Maps for report 3 ##
#######################
DAY = 230
## Infectious: p025
load(simFiles_r3s1[1])
hexAll$I  = I[which(tVec==DAY),]
mapI_rep3_p025 = mapBase
if(any(hexAll$I > 0)) {
  mapI_rep3_p025 = mapI_rep3_p025 + tm_shape(hexAll %>% filter(hexAll$I > 0)) + tm_polygons(col="I", border.alpha=1)
}
mapI_rep3_p025 = mapI_rep3_p025 + mapOverays + tm_layout(main.title = paste0("Optimistic (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)
## Infectious: median
load(simFiles_r3s1[2])
hexAll$I  = I[which(tVec==DAY),]
mapI_rep3_med = mapBase
if(any(hexAll$I > 0)) {
  mapI_rep3_med = mapI_rep3_med + tm_shape(hexAll %>% filter(hexAll$I > 0)) + tm_polygons(col="I", border.alpha=1)
}
mapI_rep3_med = mapI_rep3_med + mapOverays + tm_layout(main.title = paste0("Average (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)
## Infectious: p975
load(simFiles_r3s1[3])
hexAll$I  = I[which(tVec==DAY),]
mapI_rep3_p975 = mapBase
if(any(hexAll$I > 0)) {
  mapI_rep3_p975 = mapI_rep3_p975 + tm_shape(hexAll %>% filter(hexAll$I > 0)) + tm_polygons(col="I", border.alpha=1)
}
mapI_rep3_p975 = mapI_rep3_p975 + mapOverays + tm_layout(main.title = paste0("Pessimistic (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)


setwd(figsDir)
pdf(file="simulatedPredictionsMapI.pdf")
tmap_arrange(mapI_rep1_p025, mapI_rep1_med, mapI_rep1_p975,
             mapI_rep2_p025, mapI_rep2_med, mapI_rep2_p975,
             mapI_rep3_p025, mapI_rep3_med, mapI_rep3_p975)
dev.off()
