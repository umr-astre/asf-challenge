# source(here::here("nimble/model11/fitWildBoarModel11.R"))
rm(list=ls()) # If sourcing this will ensure the cModel is rebuilt (needed because line 172 could be dangerous)
iNSim = 1
print(iNSim)

source(here::here("src/packages.R"))
source(here::here("src/functions.R"))
source(here::here("nimble/nimbleFunctions.R"))
source(here::here("nimble/model11/model_definition.R"))
options(width=1000)
# drake::r_make()            # TO UPDATE INIT AND CONST

## iAttractI       <- 9 # 1:9
## (attractIVec    <- ilogit(seq(-4,4,by=1)))
## (iAttractIFixed <- attractIVec[iAttractI])
## (logit_attractI <- logit(iAttractIFixed))

## nimPrint("iAttractI=",iAttractI, " logit(attractI)=",logit_attractI)

(baseDir <- here::here("nimble/model11"))
setwd(baseDir)
## (mcmcDir <- paste0(baseDir, "/MCMC_lpAI",logit_attractI))


nSimVec  = 2 * c(500, 500, 500, 500, 500)
nSim     = nSimVec[iNSim]
(mcmcDir = paste0(baseDir, paste0("/MCMC",1:length(nSimVec))[iNSim])) ## "/MCMC2"))

nimPrint("nSim=",nSim)
nimPrint("mcmcDir=",mcmcDir)

if (!file.exists(mcmcDir))
  system(paste("mkdir", mcmcDir))

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
## Choose which model to use - all hexagons or a subset ####
##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
loadd(admin,
      bboxAll,
      hexSub,
      hexAll,
      hexCentroidsSub,
      fence,
      weightTauAHome1km,
      weightTauAAway1km,
      weightTauAHome2km,
      weightTauAAway2km,
      ## probNeighAS1km,
      ## probNeighAS2km,
      outbreaks_at_D110,
      stepsPerChunkSub,
      wildBoarArraySub,  # observations aggregated over ten day intervals, hexagons restricted to 20km buffer
      wildBoarObsSub,
      pig_sites)

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
## Initial values and constants ####
##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tStopSub <- 110
initWildboarSub  <- setWildBoarModelInitialValues(wbArray4sim        = wildBoarArraySub,
                                                  wildBoarObs        = wildBoarObsSub,
                                                  stepsPerChunk      = stepsPerChunkSub,
                                                  bboxAll            = bboxAll) # For fitting MUST OMIT tStop ARGUMENT
constWildboarSub <- setWildBoarModelConstants    (wbArray4sim        = wildBoarArraySub,
                                                  Hex                = hexSub,
                                                  HexCentroids       = hexCentroidsSub,
                                                  Outbreaks          = outbreaks_at_D110,
                                                  stepsPerChunk      = stepsPerChunkSub,
                                                  bboxAll            = bboxAll,
                                                  tStop              = tStopSub,
                                                  weightTauAHome1km  = weightTauAHome1km,
                                                  weightTauAAway1km  = weightTauAAway1km,
                                                  weightTauAHome2km  = weightTauAHome2km,
                                                  weightTauAAway2km  = weightTauAAway2km)
                                                  # probNeighAS1km = probNeighAS1km,
                                                  # probNeighAS2km = probNeighAS2km

##%%%%%%%%%%%%%%%%%%%
## Build R model ####
##%%%%%%%%%%%%%%%%%%%
rWildBoarModelSub = nimbleModel(bugsBoarCode,
                                constants=constWildboarSub,
                                inits=initWildboarSub,
                                calculate = FALSE, debug = FALSE) # TRUE

simulate(rWildBoarModelSub, c("scTIntro", "tIntro"))
nimPrint("Sub: tIntro = ", rWildBoarModelSub$tIntro)

##%%%%%%%%%%%%%%%%%%%%%%
## Useful node sets ####
##%%%%%%%%%%%%%%%%%%%%%%
detNodes      = rWildBoarModelSub$getNodeNames(determOnly=TRUE)
obsNodes      = sub("\\[.*","",detNodes[grep("obsY", detNodes)])
detNodesNoObs =                detNodes[grep("obsY", detNodes, invert=TRUE)]
stochNodes    = rWildBoarModelSub$getNodeNames(stochOnly=TRUE)

#################################
## Define Target & Fixed nodes ##
#################################
Fixed  <- c("logit_pR", "radiusActiveSearch")
Target <- sort(c(
  "logit_pHuntY",   ## Not good idea to fix this - it is too low.
  "logit_scTIntro",
  "logit_pIntroX",
  "logit_pIntroY",
  "logit_pHome",
  "logit_attractI",
  "logit_omegaFence",
  "log_tauP",
  "log_tauPArmy",
  "log_tauA",
  "log_tauHArmy",
  "log_beta"
))
(Target)
Monitors <- Target


##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
## Initial sub-optimal initialisation ####
##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (TRUE) { # FALSE
  samplesFile  <- paste0(mcmcDir,"/samples.csv")
  if (file.exists(samplesFile)) {
    nimPrint("Using ", samplesFile)
    previousSamples <- read.table(file=samplesFile, header = TRUE)
    for (iTarget in 1:length(Target)) {
      if (is.element(Target[iTarget], colnames(previousSamples)))
        rWildBoarModelSub[[Target[iTarget]]] <- tail(previousSamples[,Target[iTarget]],1)
      nimPrint(Target[iTarget], " = ", rWildBoarModelSub[[Target[iTarget]]])
    }
  } else {
    model1SamplesFile <- "/home/david/asf-challenge/nimble/model10/MCMCall/samples.csv"
    nimPrint("Using ", model1SamplesFile)
    previousSamples <- read.table(file=model1SamplesFile, header = TRUE)
    previousSamples <- tail(previousSamples,1)
    (rWildBoarModelSub$logit_scTIntro   <- previousSamples$logit_scTIntro)
    (rWildBoarModelSub$logit_pIntroX    <- previousSamples$logit_pIntroX)
    (rWildBoarModelSub$logit_pIntroY    <- previousSamples$logit_pIntroY)
    (rWildBoarModelSub$logit_pHome      <- previousSamples$logit_pHome)
    (rWildBoarModelSub$logit_attractI   <- previousSamples$logit_attractI)
    (rWildBoarModelSub$logit_omegaFence <- previousSamples$logit_omegaFence)
    (rWildBoarModelSub$log_tauP         <- previousSamples$log_tauP)
    (rWildBoarModelSub$log_tauPArmy     <- previousSamples$log_tauPArmy)
    (rWildBoarModelSub$log_tauA         <- previousSamples$log_tauA)
    (rWildBoarModelSub$log_tauHArmy     <- previousSamples$log_tauHArmy)
    (rWildBoarModelSub$log_beta         <- previousSamples$log_beta)
    (iniPropCov                         <- diag(1E-3, length(Target)))
  }
  (rWildBoarModelSub$logit_pR         <- logit(0.05))
}


##%%%%%%%%%%%%%%%%%%%%
## Configure MCMC ####
##%%%%%%%%%%%%%%%%%%%%
(simulate(rWildBoarModelSub, detNodesNoObs))
rWildBoarModelSub$tauH
rWildBoarModelSub$tauHArmy
rWildBoarModelSub$omegaFence
rWildBoarModelSub$attractI

val <- {}
for (tt in Target) {
  val[tt] <- eval(parse(text=paste0("rWildBoarModelSub$", tt)))
  nimPrint(tt, " = ", val[tt])
}

##%%%%%%%%%%%%%%%%%%%%
## Compile models ####
##%%%%%%%%%%%%%%%%%%%%
if (is.element("cWildBoarModelSub", ls())) {
  nimPrint("WARNING: NOT RECOMPILING cWildBoarModelSub. USING PARAMETERS FROM rWildBoarModelSub. VERIFY THAT THIS IS DESIRED BEHAVIOUR!!!")
  nimCopy(from=rWildBoarModelSub, to=cWildBoarModelSub, nodes = stochNodes)
} else {
  nimPrint("RECOMPILING cWildBoarModelSub")
  cWildBoarModelSub = compileNimble(rWildBoarModelSub, showCompilerOutput = TRUE) # FALSE
}

#####################################################################################
## Visual Check on Simulation - CAREFUL TO REPLACE SIMUALTED DATA WITH ORIGINAL!!! ##
#####################################################################################
if (TRUE) { # FALSE
  # simulate(rWildBoarModelSub, detNodes)
  simulate(cWildBoarModelSub, detNodes)
  fNames <- c("1HN","2HP","3AS","4PS")
  for (tenDayChunk in 1:9) {
    print(data.frame(data=c(sum(wildBoarArraySub[1,tenDayChunk,]),
                            sum(wildBoarArraySub[2,tenDayChunk,]),
                            sum(wildBoarArraySub[3,tenDayChunk,]),
                            sum(wildBoarArraySub[4,tenDayChunk,])),
                     model=c(sum(cWildBoarModelSub$obsY[1,tenDayChunk,]),
                             sum(cWildBoarModelSub$obsY[2,tenDayChunk,]),
                             sum(cWildBoarModelSub$obsY[3,tenDayChunk,]),
                             sum(cWildBoarModelSub$obsY[4,tenDayChunk,])),
                     row.names = c("HN", "HP", "AS", "PS")))
  }
  if (FALSE) { # TRUE
    for (tenDayChunk in 1:8) {
      for (iCol in 1:4) {
        nimPrint(tenDayChunk, " ", iCol)
        mapInfectiousDayX(t(rWildBoarModelSub$obsY[iCol,,]), Hex=hexSub,
                          Admin=admin%>%filter(is.element(rowAdmin, c(6,9,11,22))),
                          Day=tenDayChunk, outputDir="/home/david/asf-challenge/nimble/model11/checks",
                          fileName=paste0("check_chunk", tenDayChunk, "_", fNames[iCol], "_data"),
                          StepsPerChunk=10, Fence=fence, pig_sites=pig_sites, Outbreaks=outbreaks_at_D110)
        mapInfectiousDayX(t(cWildBoarModelSub$obsY[iCol,,]), Hex=hexSub,
                          Admin=admin%>%filter(is.element(rowAdmin, c(6,9,11,22))),
                          Day=tenDayChunk, outputDir="/home/david/asf-challenge/nimble/model11/checks",
                          fileName=paste0("chunk", tenDayChunk, "_", fNames[iCol], "_sim"),
                          StepsPerChunk=10, Fence=fence, pig_sites=pig_sites, Outbreaks=outbreaks_at_D110)
      }
    }
  }
}

##%%%%%%%%%%
## MCMC ####
##%%%%%%%%%%
reconfigure <- TRUE
allowResets <- TRUE # FALSE   ## Set to TRUE prior to converging, FALSE once stable
nIter <- 1000
for (iRun in 1:100) { # iRun=1
  nimPrint("nSim=",nSim)
  nimPrint("mcmcDir=",mcmcDir)
  samplesFile  <- paste0(mcmcDir,"/samples.csv")
  samples2File <- paste0(mcmcDir,"/samples2.csv")
  samplesExist <- file.exists(samplesFile) & file.exists(samples2File)
  if (samplesExist) {
    loglikSamples <- as.mcmc(read.table(file=samples2File, header = TRUE))
    ess <- effectiveSize(loglikSamples)
    nimPrint("Effective size of loglikSamples = ", ess)
    if (iRun==1 | (allowResets & ess < 10)) {
      ##################################################
      ## VERIFY OBSERVED DATA ARE IN TACT - IMPORTANT ##
      ##################################################
      rWildBoarModelSub$obsY[,,] <- wildBoarArraySub[,,]
      cWildBoarModelSub$obsY[,,] <- wildBoarArraySub[,,]
      ##%%%%%%%%%%%%%%%%%%%%%%%%%%%
      ## Load previous samples ####
      ##%%%%%%%%%%%%%%%%%%%%%%%%%%%
      previousSamples <- read.table(file=samplesFile, header = TRUE)
      previousSamples <- tail(previousSamples, n=round(nrow(previousSamples)/3))
      for (iTarget in 1:length(Target)) {
        if (is.element(Target[iTarget], colnames(previousSamples)))
          rWildBoarModelSub[[Target[iTarget]]] <- tail(previousSamples[,Target[iTarget]],1)
        nimPrint(Target[iTarget], " = ", rWildBoarModelSub[[Target[iTarget]]])
      }
      nimCopy(from=rWildBoarModelSub, to=cWildBoarModelSub, nodes = stochNodes)
      simulate(cWildBoarModelSub, detNodesNoObs)
      ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      ## Reinitialise Cov Matrix ####
      ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      iniPropCov <- cov(previousSamples)
      diag(iniPropCov)
      while (any(0>eigen(iniPropCov)$values)) {
        scalePropCov <- 1.1
        diag(iniPropCov) <- diag(iniPropCov) * scalePropCov
        iniPropCov <- iniPropCov / scalePropCov
      }
      diag(iniPropCov)
      nimPrint(iniPropCov)
      nimPrint(cov2cor(iniPropCov))
      reconfigure <- TRUE
    }
  }
  if (iRun==1 | reconfigure) {
    ##%%%%%%%%%%%%%%%%%%%%%%%
    ## Reinitialise MCMC ####
    ##%%%%%%%%%%%%%%%%%%%%%%%
    mcPoissonLoglik <- setupMcPoissonLoglik(model=rWildBoarModelSub, obsYNode='obsY', obsY = wildBoarArraySub,
                                            constants = list(nSim=nSim, Log=TRUE), targetNodes = Target, fixedNodes = Fixed)
    mcmcConf <- configureMCMC(rWildBoarModelSub, monitors=Monitors, monitors2="loglik", nodes = NULL)
    mcmcConf$printSamplers()
    mcmcConf$getMonitors()
    mcmcConf$getMonitors2()
    mcmcConf$removeSamplers()
    mcmcConf$addSampler(target = Target, type = 'sampler_RW_llFunction_block_custom', ## sampler_RW_llFunction_block_custom - this gives "could not find function "extractControlElement"" which is nuts
                        control = list(llFunction = mcPoissonLoglik,
                                       includesTarget = FALSE,     ## FALSE tells sampler to add prior weights for target
                                       propCov = iniPropCov,
                                       scale = ifelse(samplesExist, 1.0, 0.01), ## Default=1 - works bad when initial covProp is terrible
                                       ## adaptFactorExponent = 0.7,  ## Default=0.8
                                       adaptive = TRUE,
                                       adaptInterval=200))         ## Default = 200
    print(mcmcConf)
    Cmcmc = compileNimble(buildMCMC(mcmcConf))
    reconfigure <- FALSE
  }
  ##%%%%%%%%%%%%%%
  ## Run MCMC ####
  ##%%%%%%%%%%%%%%
  STime <- run.time(Cmcmc$run(nIter, reset=FALSE))  ## 23h22
  ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ## Import into coda - filtering a NA line that can be generated when recompiling the modelVal##ues object ##
  ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  samples <- as.matrix(Cmcmc$mvSamples)
  samples <- samples[samples[,"logit_pHome"]!=0,]
  if(any(is.na(samples[1,])))
    samples <- samples[-1,]
  samples  <- as.mcmc(samples)
  ##
  samples2 <- as.matrix(Cmcmc$mvSamples2)
  samples2 <- samples2[samples2[,"loglik"]!=0,]
  if(is.na(samples2[1]))
    samples2 <- samples2[-1]
  samples2 <- as.mcmc(samples2)
  ##%%%%%%%%%%%%%%%%%%%%
  ## Plot with CODA ####
  ##%%%%%%%%%%%%%%%%%%%%
  pdf(file=paste0(mcmcDir,"/trajectories_",nSim,".pdf"))
  plot(samples)
  dev.off()
  ##
  pdf(file=paste0(mcmcDir,"/trajectories-loglik_",nSim,".pdf"))
  plot(samples2)
  dev.off()
  ##
  write.table(samples,  file=paste0(mcmcDir,"/samples.csv"), row.names = FALSE)
  write.table(samples2, file=paste0(mcmcDir,"/samples2.csv"),row.names = FALSE)
  ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ## Check marginalised simulated values ####
  ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  for (tenDayChunk in 1:9) {
    print(data.frame(data=c(sum(wildBoarArraySub[1,tenDayChunk,]),
                            sum(wildBoarArraySub[2,tenDayChunk,]),
                            sum(wildBoarArraySub[3,tenDayChunk,]),
                            sum(wildBoarArraySub[4,tenDayChunk,])),
                     model=c(sum(cWildBoarModelSub$obsY[1,tenDayChunk,]),
                             sum(cWildBoarModelSub$obsY[2,tenDayChunk,]),
                             sum(cWildBoarModelSub$obsY[3,tenDayChunk,]),
                             sum(cWildBoarModelSub$obsY[4,tenDayChunk,])),
                     row.names = c("HN", "HP", "AS", "PS")))
  }
  ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ## Check latest simulation ####
  ##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if (FALSE) {
    for (tenDayChunk in 1:8) {
      for (iCol in 1:4) {
        nimPrint(tenDayChunk, " ", iCol)
        mapInfectiousDayX(t(wildBoarArraySub[iCol,,]), Hex=hexSub,
                          Admin=admin%>%filter(is.element(rowAdmin, c(6,9,11,22))),
                          Day=tenDayChunk, outputDir="/home/david/asf-challenge/nimble/model11/checks",
                          fileName=paste0("chunk", tenDayChunk, "_", fNames[iCol], "_data"),
                          StepsPerChunk=10, Fence=fence, pig_sites=pig_sites, Outbreaks=outbreaks_at_D110)
        mapInfectiousDayX(t(cWildBoarModelSub$obsY[iCol,,]), Hex=hexSub,
                          Admin=admin%>%filter(is.element(rowAdmin, c(6,9,11,22))),
                          Day=tenDayChunk, outputDir="/home/david/asf-challenge/nimble/model11/checks",
                          fileName=paste0("chunk", tenDayChunk, "_", fNames[iCol], "_sim"),
                          StepsPerChunk=10, Fence=fence, pig_sites=pig_sites, Outbreaks=outbreaks_at_D110)
      }
    }
  }
}





### What prior can stop tauP shrinking to 0 and remain realistic ?
## M  <- 0
## SD <- 3
## (yMax <- qlnorm(0.99,M,SD)) ## exp(M+SD^2)
## par(mfrow=n2mfrow(2))
## curve(dlnorm(x,M,SD),0,yMax, n=1001)
## (R <- exp(M - SD^2)) ## Mode of lnorm
## ## Is mode reasonable?
## (yMax <- qexp(0.99,R)) ## exp(M+SD^2)
## curve(pexp(x,rate=R),0, yMax, ylim=0:1)
## qexp(0.99,rate=R) / 365          # 102.2358 years to have 99% prob of detection
## pgeom(yMax,pexp(1,rate=R))       # 0.9889418
## qgeom(0.99,pexp(1,rate=R)) / 365 # 102.2356 years to have 99% prob of detection
## log(R) # -9.21034
