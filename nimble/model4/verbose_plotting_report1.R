## Adapted from model12/verbose_plotting.R
library(sf)
library(tmap)
library(tmaptools)
library(dplyr)
library(here)

############################################################################################
## Set the working directory to the directory storing the simulation output (Rdata) files ##
############################################################################################
setwd(here("nimble/model4/newSims_report1"))

############################################################
## Load R objects required for mapping the simualted data ##
############################################################
load(here("nimble/objectsForMapping.Rdata"))

##############################################################
## Choose SCENARIO and identify associated simulation files ##
##############################################################
SCENARIO <- 1 # choose one of 1, 3, 4 (not 2 - they won't use it)
simFiles  = sort(dir()[grep(paste0("scenario",SCENARIO), dir())])
simFiles  = simFiles[grep("Rdata", simFiles)]
nSimFiles = length(simFiles)
if (nSimFiles != 10000) stop("There should be 10000 simualtion files")

scenarios           <- cbind(c(1.0,1.0,0.98,0.0),c(60, 55, 60, 365))# rbind(c(0, 365), expand.grid(omegaFenceVec, tFenceVec))
colnames(scenarios) <- c("omegaFence","tFence")
scenarios

########################
## Set some constants ##
########################
tStop      = 100
nSim       = length(simFiles)
omegaFence = scenarios[SCENARIO,1]
tFence     = scenarios[SCENARIO,2]

#################################################################################
## Plot trajectories of 6 hidden states and 3 observed states (detected cases) ##
#################################################################################
nSim4plot = 10
tRange    = -30:tStop
ALPHA     = 0.2
par(mfrow=n2mfrow(9), cex.axis=1.3, cex.lab=1.3)
## Susceptible
plot(tRange, tRange, typ="n", ylab="Susceptible", xlab="Day", ylim=c(0,270000))
abline(h=0)
for (sim in 1:nSim4plot) { # sim=1
  if (sim %% round(nSim4plot/10) == 1) print(sim)
  load(simFiles[sample(nSim, 1)]) # Choose one file at random & load it
  ## Correct first row of S which was set to zero-vector prior to epideic
  S[1,] = S[2,]
  tFill = min(tRange):min(tVec)
  lines(tFill, rep(sum(S[1,]),length(tFill)) , col=rgb(0,0,0,ALPHA)) ## Plots constant density prior to start of epidemic
  lines(tVec, rowSums(S), col=rgb(0,0,0,ALPHA))
  abline(v=0, col="lightpink")
}
## Incidence (new exposed cases)
plot(tRange, tRange, typ="n", ylab="Incidence", xlab="Day", ylim=c(0,800))
abline(h=0)
for (sim in 1:nSim4plot) { # sim=1
  if (sim %% round(nSim4plot/10) == 1) print(sim)
  load(simFiles[sample(nSim, 1)]) # Choose one file at random & load it
  lines(tVec, rowSums(newE), col=rgb(0,0,0,ALPHA))
  abline(v=0, col="lightpink")
}
## Exposed
plot(tRange, tRange, typ="n", ylab="Exposed", xlab="Day", ylim=c(0,2200))
abline(h=0)
for (sim in 1:nSim4plot) { # sim=1
  if (sim %% round(nSim4plot/10) == 1) print(sim)
  load(simFiles[sample(nSim, 1)]) # Choose one file at random & load it
  lines(tVec, rowSums(E), col=rgb(0,0,0,ALPHA))
  abline(v=0, col="lightpink")
}
## Infectious
plot(tRange, tRange, typ="n", ylab="Infectious", xlab="Day", ylim=c(0,1800))
abline(h=0)
for (sim in 1:nSim4plot) { # sim=1
  if (sim %% round(nSim4plot/10) == 1) print(sim)
  load(simFiles[sample(nSim, 1)]) # Choose one file at random & load it
  lines(tVec, rowSums(I), col=rgb(0,0,0,ALPHA))
  abline(v=0, col="lightpink")
}
## Recovered
plot(tRange, tRange, typ="n", ylab="Recovered", xlab="Day", ylim=c(0,1900))
abline(h=0)
for (sim in 1:nSim4plot) { # sim=1
  if (sim %% round(nSim4plot/10) == 1) print(sim)
  load(simFiles[sample(nSim, 1)]) # Choose one file at random & load it
  lines(tVec, rowSums(R), col=rgb(0,0,0,ALPHA))
  abline(v=0, col="lightpink")
}
## Carcass
plot(tRange, tRange, typ="n", ylab="Carcass", xlab="Day", ylim=c(0,13000))
abline(h=0)
for (sim in 1:nSim4plot) { # sim=1
  if (sim %% round(nSim4plot/10) == 1) print(sim)
  load(simFiles[sample(nSim, 1)]) # Choose one file at random & load it
  lines(tVec, rowSums(C), col=rgb(0,0,0,ALPHA))
  abline(v=0, col="lightpink")
}
## Hunted & tested +ve
plot(tRange, tRange, typ="n", ylab="Hunted & Tested +ve", xlab="Day", ylim=c(0,25))
abline(h=0)
for (sim in 1:nSim4plot) { # sim=1
  if (sim %% round(nSim4plot/10) == 1) print(sim)
  load(simFiles[sample(nSim, 1)]) # Choose one file at random & load it
  lines(tVec, rowSums(HP), col=rgb(0,0,0,ALPHA))
  abline(v=0, col="lightpink")
}
## Active search
plot(tRange, tRange, typ="n", ylab="Active search", xlab="Day", ylim=c(0,50))
abline(h=0)
for (sim in 1:nSim4plot) { # sim=1
  if (sim %% round(nSim4plot/10) == 1) print(sim)
  load(simFiles[sample(nSim, 1)]) # Choose one file at random & load it
  lines(tVec, rowSums(AS), col=rgb(0,0,0,ALPHA))
  abline(v=0, col="lightpink")
}
## Passive search
plot(tRange, tRange, typ="n", ylab="Passive search", xlab="Day", ylim=c(0,25))
abline(h=0)
for (sim in 1:nSim4plot) { # sim=1
  if (sim %% round(nSim4plot/10) == 1) print(sim)
  load(simFiles[sample(nSim, 1)]) # Choose one file at random & load it
  lines(tVec, rowSums(PS), col=rgb(0,0,0,ALPHA))
  abline(v=0, col="lightpink")
}


###################################################
## Mapping - of one randomly selected simulation ##
###################################################
# drake::loadd(hexAll, hexSub_at_D110, fence, huntZone, admin)
# save(hexAll, hexSub_at_D110, fence, huntZone, admin, file="objectsForMapping.Rdata")
# load(here("nimble/objectsForMapping.Rdata"))

## CHOOSE ONE FILE AT RANDOM & LOAD IT
load(simFiles[sample(nSim, 1)])
## Select simulated data from one given day
DAY         = 80
ZOOM        = TRUE
hexAll$S    = S[which(tVec==DAY),]
hexAll$E    = E[which(tVec==DAY),]
hexAll$I    = I[which(tVec==DAY),]
hexAll$R    = R[which(tVec==DAY),]
hexAll$C    = C[which(tVec==DAY),]
hexAll$newE = newE[which(tVec==DAY),]
hexAll$HP   = HP[which(tVec==DAY),]
hexAll$AS   = AS[which(tVec==DAY),]
hexAll$PS   = PS[which(tVec==DAY),]
## Define map extent
if (ZOOM==TRUE) {
  mapBase = tm_shape(hexSub_at_D110) + tm_polygons(col = "pForest", lwd=0.2, border.col="grey", palette=get_brewer_pal("Greens", n = 10, plot=FALSE))
} else {
  mapBase = tm_shape(hexAll) + tm_polygons(col = "pForest", lwd=0.2, border.col="grey", palette=get_brewer_pal("Greens", n = 10, plot=FALSE))
}
## Define finishing overlays
mapOverays =
  tm_shape(admin) + tm_borders(lwd=2) +
  tm_shape(fence) + tm_borders(col="darkviolet", lwd=2) +
  tm_shape(huntZone) + tm_borders(col="red", lwd=2)
## Map of susceptibles
mapS = mapBase
if(any(hexAll$E>0)) {
  mapS = mapS + tm_shape(hexAll %>% filter(hexAll$S > 0)) + tm_polygons(col="S", border.alpha=1)
}
mapS = mapS + mapOverays + tm_layout(main.title = paste0("Susceptibles (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)
## Map of exposed
mapE = mapBase
if(any(hexAll$E>0)) {
  mapE = mapE + tm_shape(hexAll %>% filter(hexAll$E > 0)) + tm_polygons(col="E", border.alpha=1)
}
mapE = mapE + mapOverays + tm_layout(main.title = paste0("Exposed (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)
## Map of infectious
mapI = mapBase
if(any(hexAll$I>0)) {
  mapI = mapI +
    tm_shape(hexAll %>% filter(hexAll$I > 0)) + tm_polygons(col="I", border.alpha=1)
}
mapI = mapI + mapOverays + tm_layout(main.title = paste0("Infectious (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)
## Map of Carcasses
mapC = mapBase
if(any(hexAll$C>0)) {
  mapC = mapC +
    tm_shape(hexAll %>% filter(hexAll$C > 0)) + tm_polygons(col="C", border.alpha=1)
}
mapC = mapC + mapOverays + tm_layout(main.title = paste0("Carcasses (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)
## Map of Recovered
mapR = mapBase
if(any(hexAll$R>0)) {
  mapR = mapR +
    tm_shape(hexAll %>% filter(hexAll$R > 0)) + tm_polygons(col="R", border.alpha=1)
}
mapR = mapR + mapOverays + tm_layout(main.title = paste0("Recovered (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)
## Map of Incidence (new exposed cases)
mapNewE = mapBase
if(any(hexAll$newE>0)) {
  mapNewE = mapNewE +
    tm_shape(hexAll %>% filter(hexAll$newE > 0)) + tm_polygons(col="newE", border.alpha=1)
}
mapNewE = mapNewE + mapOverays + tm_layout(main.title = paste0("New cases (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)
## Map of Hunted & tested +ve
mapHP = mapBase
if(any(hexAll$HP>0)) {
  mapHP = mapHP +
    tm_shape(hexAll %>% filter(hexAll$HP > 0)) + tm_polygons(col="HP", border.alpha=1)
}
mapHP = mapHP + mapOverays + tm_layout(main.title = paste0("Hunted ASF cases (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)
## Map of Active search
mapAS = mapBase
if(any(hexAll$AS>0)) {
  mapAS = mapAS +
    tm_shape(hexAll %>% filter(hexAll$AS > 0)) + tm_polygons(col="AS", border.alpha=1)
}
mapAS = mapAS + mapOverays + tm_layout(main.title = paste0("Active search case (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)
## Map of Passive search
mapPS = mapBase
if(any(hexAll$PS>0)) {
  mapPS = mapPS +
    tm_shape(hexAll %>% filter(hexAll$PS > 0)) + tm_polygons(col="PS", border.alpha=1)
}
mapPS = mapPS + mapOverays + tm_layout(main.title = paste0("Passive search cases (day ", DAY, ")"), main.title.size = 1, legend.outside = TRUE)
## Plot all maps
tmap_arrange(mapS, mapNewE, mapE, mapI, mapC, mapR, mapHP, mapAS, mapPS)
