<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>ASF Challenge</title>
    <meta charset="utf-8" />
    <meta name="author" content="Astre Team, Montpellier, France." />
    <meta name="date" content="2022-04-21" />
    <script src="libs/header-attrs/header-attrs.js"></script>
    <link href="libs/remark-css/default.css" rel="stylesheet" />
    <link href="libs/remark-css/default-fonts.css" rel="stylesheet" />
    <link rel="stylesheet" href="libs/font-awesome/css/fontawesome-all.min.css" type="text/css" />
    <link rel="stylesheet" href="cirad.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">












class: title-slide, inverse
background-image: url(img/presentation-title-background.png)
background-size: cover


![:scale 20%](img/asf-challenge_logo.png)

# ASF Challenge

## Modelling approach

### Astre Team, Montpellier, France.






---
# Team members

![](img/member_ferran.jpg)&lt;br/&gt;
Ferran Jori
.light[Veterinary epidemiologist&lt;br/&gt;
Team representative]

.pull-left[
![](img/member_david.jpg)&lt;br/&gt;
David Pleydell&lt;br/&gt;
.light[Mathematical ecologist]
]

.pull-right[
![:scale 20%](img/member_facu.jpg)&lt;br/&gt;
Facundo Muñoz&lt;br/&gt;
.light[Statistician]
]



---
# Introduction

![:scale 120%](img/relationship_data-models.svg)


---
class: inverse, middle, center

# Farm infection model

???

First, we are going to present the model of farm infection.

---
# Overview

Estimates the __probability of ASF infection__ over the next __prediction period__, for __each pig herd__.

.pull-left[

- Estimates specific infection probability estimates for __three transmission pathways__.

- Combines probabilities assuming __independence__.

- Prediction periods starting at D50, D80 and D110.
]

.pull-right[
![:scale 200%](img/model-farms-summary.svg)
]


???

We considered 3 possible transmission pathways of farm contamination: contact with infectious wild boar, trading of live pigs and fomites.

We aggregated the probabilities of transmission from each pathway into a total __probability__ of infection for each individual farm, over the next period, with data up to days 50, 80 and 110.












---
# Transmission via infectious wild boar - exposure level

.pull-left[
__Kernel density smoothing__ of observed __wild boar cases__ (.red[•]) up to D50, D80 and D110.

The density value at each __outdoor farm__ (.white[⏺]) was used as a measure of __exposure__ to infectious wild boar.

__Assumption__: only outdoor farms are susceptible to infectious WB.
]

.pull-right[
![](presentation_astre_files/figure-html/unnamed-chunk-1-1.png)&lt;!-- --&gt;
]

???

For quantifying the risk of transmission by contact with infectious wild boar, we considered the __density__ of the observed cases as a measure of the local exposure level.

The red points in the figure represent the locations of the recorded wild boar cases up to day 80.
The fence and the buffer zone are outlined for reference and the white circles are the location of the __outdoor farms__ in the area. Infected and detected farms by day 80 are identified with a interior black bullet and a label with their ID.

A strong assumption was that only outdoor farms were susceptible to this transmission pathway.


---
# Transmission via infectious wild boar - model


.pull-left[
Use the __exposure level__ as covariate in a
__logistic regression__ for the
__detection status__ of __outdoor__ farms
in the region at risk.

Estimate __probability of infection and detection__ for each farm, in the observed period.



]

.pull-right[

![](presentation_astre_files/figure-html/prob-farm-detection-1.png)&lt;!-- --&gt;

]


???

We then fitted a logistic regression for the infection status of the farms given the level of exposure.
The figure in the right displays the estimated regression at D110.

Note that the estimated probabilities considers the risk cumulated over the entire observation period, since the first case up to the current date. They represent the probability of infection __during__ the observed period.


---
# Transmission via infectious wild boar - prediction


.pull-left[
__Daily infection probabilities__  derived:

- __assuming__ homogeneity and independence;
- __neglecting__ culling periods.


Used to __predict__ :
- infection probabilities over the next period;
- effects of __control strategies__.

]

.pull-right[
![](img/prob-inf-wb.svg)
]

???

We derive an estimate of the __daily__ probability of infection, based on the size in days of the observation period, assuming constant risk and independence across days.

This is effectively as if we threw the same dice every day to decide whether a farm gets contaminated.

Reversing the calculation, we computed the probability of infection for the farm in the __prediction period__ using the same assumption of homogeneity and independence.
We will discuss these assumptions in the conclusions.

Here we removed the culled periods from the number of days at risk in the prediction period.

The results change with some of the control strategies.
Specifically culling farms sites in the vicinity of wild boar cases, or within the protection zone, simply because they reduce the number of days at risk.

And this is how we obtained our estimate of the infection probability from exposure to infectious wild boar.


---
# Transmission via trade - overview


.pull-left[
Each __farm__ risks receiving an __infected shipment__ from:

- __observed__ recent shipments
- potential __future__ shipments.

]

.pull-right[
![](img/farm-shipments.svg)
]


???

Concerning the trade pathway, for each __farm__, we consider the risk of receiving an __infected shipment__ from any other farm.

Notice that such a transmission must either has occurred in one of the shipments from the last 15 days (but not earlier, since it would have been already detected at the destination by now), or it will happen in one future shipment during the prediction period.

For the observed shipments, we must assess the probability that either one of them included infected animals, whereas in the future there is the additional uncertainty about the actual shipments to take place.

Also note that shipments can potentially originate from infected farms, if they took place in the 7 days before symptom onset at the source.


---
# Transmission via trade - observed shipments


.pull-left[
.small[
Considered all shipments received in the __last 15 days__ (detection period):

- made within __7 days__ (symptom onset period) prior to __detection__ `\((p = 0.8)\)` at __infected farms__;

- for __farms at risk__ we used the  __probability of infection__ over the __15 days__ prior to shipment.
]
]

.pull-right[
![](img/prob-inf-trade-observed.svg)
]


???

For each target farm, we look at all received shipments in the last 15 days from farms that were later (before 7 days) detected as infected (if any).
This never actually occurred during the challenge.

Of course, once detected, farms were banned from trading and there was no further risk of dissemination.

For a recent shipment from a farm at risk, we considered that the probability of transmission equals the probability of infection of the shipping farm in the period from 15 days before today and the date of the shipment.

We can compute that probability from the daily probability of infection of the shipping farm, computed previously.

Finally, we combined additively the individual probabilities of infection for each observed shipment with destination to the target farm to compute the expected number of infected shipments.



---
# Transmission via trade - predicted shipments


.pull-left[
.small[
- For each __origin__-__destination__,
`$$\mathbb{P}(\text{inf. ship.}) = \mathbb{P}(\text{ship.}) \cdot \mathbb{P}(\text{inf.} | \text{ship.})$$`

- Shipments from A to B:
`$$N_{AB} \sim \text{Poisson}(\lambda_{AB})$$`

  - `\(\hat\lambda_{AB}\)`: observed __rates__

- `\(\mathbb{P}(\text{inf.} | \text{ship.}) = \frac12[1 - (1-p_d)^{n_d}]\)`

- Exp. number of inf. shipments:
`$$\hat\lambda_{AB}  \cdot \mathbb{P}(\text{inf.} | \text{ship.})$$`
]
]

.pull-right[
![](img/prob-inf-trade-predicted.svg)
]


???

During the predictive period we don't know the actual shipments that are going to take place.

The probability of an infected shipment from farm A to farm B can be expressed as the probability of a shipment taking place in that period times the probability that this particular shipment is infected.

We took a rough shortcut here and simply used the empirical rates of shipments for every source and destination. Thus, If no shipment from A to B has been observed in our historical records (since two months before the first case), we assumed that the probability was 0.
On the other hand, if we had observed 2 shipments from A to B in our historical record spanning 4 months, we have a rate of 0.5 shipments per month

The probability of the shipment being infectious was evaluated from the daily probability of infection of the farm at the origin, and the number of unbanned days in the prediction period. The cumulative probability was halved to account for the fact that the shipment can take place either before or after the infection with equal chance.

In this way we computed the expected number of infected shipments in the prediction period for all target farms, by adding together the individual expectations from all the possible origins with which there have been commercial relations in the past.


---
# Transmission via trade - predicted shipments


.pull-left[
.small[
For each destination `\(j\)`, add the __expected numbers of infected shipments__ from individual farms into `\(E_j\)`

Prob. of infection via trade

`\(X \sim \mathcal{Bi}(n, p), \quad E_j = np\)`

`$$P(X&gt;0) = 1 - \big(1-\frac{E_j}{n}\big)^n \approx E_j$$`
for `\(E_j &lt; .25\)`

]
]

.pull-right[
![](img/prob-inf-trade.svg)
]


???

In summary, for every target farm `\(j\)` we have expected numbers of infectious shipments from past or future mouvements that can be simply added together into a total expectation for the prediction period `\(E_j\)`.

Considering `\(E_j\)` as the expected value of a Binomial variable of the number of infected shipments, the probability of at least 1 infected shipment is approximately equal to `\(E_j\)` for moderate values.

This approximation avoids the dependency on the total number of shipments, which is uncertain for the future.


---
# Transmission via fomites - case 2634



.pull-left[
Probability of infection of farm `\(i\)` due to __proximity__ to farm `\(j\)`

`$$p_{ij} = p_{j} e^{-d_{ij}/\lambda}$$`

- `\(p_j\)` prob. of infection by exposure to infectious wild boar
- `\(d_{ij}\)` distance in _metres_
- `\(\lambda = 700 / \log(2)\)`

]


.pull-right[
![](img/farms-fomites-1.svg)
]


???

For the second submission, we observed a newly infected farm (Id 2634) which, while in the area at risk, was not susceptible to infectious wild boar according to our framework, since it was an in-door farm. Neither it had received any shipment. Yet, it got infected.

However, it was located in close proximity (~710 m) to another infected, albeit very small, outdoor finishing farm. Perhaps, they were visited by the same veterinarian or other people that introduced the virus via their equipment or vehicles.
This suggested a fomite transmission pathway, related to __proximity__ to infected farms.

So we assumed a risk proportional to the probability of infection by wild boar of the neighbouring farm `\(j\)` weighted by an exponentially decaying function of the distance.

The calibration parameter `\(\lambda\)` was chosen so that the risk is halved every 700 `m`, after our friend 2634.

---
# Transmission via fomites - aggregation



.pull-left[

The total probability of infection by fomites
for every farm `\(i\)` was computed as

`$$p_i = 1 - \prod_{j \in \partial i} (1 - p_{ij})$$`


- __Assumption__: conditional independence
]


.pull-right[
![](img/farms-fomites.svg)
]


???

In practice, every target farm `\(i\)` has multiple neighbours representing different magnitudes of risk depending on their distances and their particular probabilities of infection.

The final estimated risk of infection via fomites from __any__ of the neighbours at risk was computed by aggregating their individual risks assuming that a transmission event from a neighbouring farm is independent from transmission from other neighbours, given the infection probabilities.

This unavoidable approximation ignores some sources of dependency, such a veterinarian or vehicles visiting several neighbouring farms, causing transmission events more likely to occur in clusters. If anything, it leads to some underestimation of the aggregated probability of infection.


---
# Farm infection probability - rejoinder

.scroll-box[
![](img/model-farms.svg)
]

???

In summary, we estimated infection probabilities for each farm, from each transmission pathway, at each prediction period.

Some of the intermediate quantities depended on the alternative control strategies, which gave us a tool to quantify their impact and provide recommendations.

---
# Farm infection model - predictions vs. observations

.pull-left[
__D51-D80__
![](presentation_astre_files/figure-html/performance-of-report1-model-1.png)&lt;!-- --&gt;


]


.pull-right[
__D81-110__
![](presentation_astre_files/figure-html/farms-pred-vs-realised-1.png)&lt;!-- --&gt;


]


???

These figures illustrate the predictive performance of the model in the two prediction periods for which we had the realised events.

Each bubble is a pig herd with area proportional to our predicted probability of infection in the corresponding period.
Actual infections are highlighted in yellow.

In the first period, we can clearly see that the three farms with higher predicted risks got actually infected, including one transmission by animal shipment.
There were also three other farms that we considered at a similar risk level, mainly due to animal transportation, but did not get infected.
This was expected, though. For predicted infection probabilities of about 0.3, we only expect a corresponding fraction of them to actually get infected.

On the other hand, there were a few farms that got infected, for which we had predicted a risk below 5%. This was again expected, for the same reason: there were so many outdoor farms in the area with small infection probabilities that necessarily a handful of them would get infected.
This is, except for the square one down here, which corresponds to our in-door herd number 2634, that lead to the introduction of the fomites pathway in the next submission.

In the second period we observe a similar pattern: the yellow herds that actually got infected had relatively high predicted risks of infection, except for one. And there were also several other farms with varying predicted risks that did not get infected.


---
class: inverse, middle, center
# Wild boar model



---
class: larger
# Wild boar compartmental model


&lt;img src="img/modelWBflowDiagram.jpg" width="1516" /&gt;

- Spatial dynamics on __hexagonal grid__ (5km pixels)
- __Neglects__ natural demographics
- Stochastic simulation via __ `\(\tau\)`-leap method__ `\((\Delta_t = 1\)` day)
- Written in __Nimble__ (C++, MCMC) [https://r-nimble.org/]



---
class: larger
# Wild boar model - population density
![:scale 85%](img/wb-density-equation.jpg)
.pull-left[
- __ `\(\omega\)` __   habitat preference (0.8)
- __ `\(pForest\)` __  proportion forest
- __ `\(H_A\)` __      hunting bag in area `\(A\)`
- __ `\(k\)` __        pixel index

$$ D_k \sim \mathcal{Poisson}( E [ D_k ] )  $$
]

.pull-right[
&lt;img src="img/mapWBDensity.jpg" width="3400" /&gt;
]



---
class: larger
# Wild boar model - force of infection
.center[![:scale 75%](img/foi-equations.jpg)]

__Connectivity__ terms account for:
- differential mobility of __living__ (_I_) &amp; __dead__ (_C_) __infection sources__;
- probability WB is in its __home pixel__ `\((p_{Home} = 1 - p_{Away})\)`.

Includes parameter for __relative attractivity__  of __ _I_ __ &amp; __ _C_ __.
- `\(\beta_I = \beta \times p_{AttractI}\)`
- `\(\beta_C = \beta \times (1 - p_{AttractI})\)`



---
# Wild boar model - active search (AS)

Globally homogeneous detection rate in report 1. Thereafter:
- Radius `\(r \in \{1,2 \}\)`.
- `\(w_{Cr}\)` &amp; `\(w_{Nr}\)` expected props. of circle in case &amp; neighbour pixels.
- __7 day delay__ from case confirmation to starting active search.
- _Detection prob_ in case pixel set to `\(p_{Cr} = \frac {w_{Cr}} {w_{C1}} (1-exp(-\tau_{1}))\)`.
- _Detection rate_ in neighbours __augmented__ by `\(-log(1- \frac {w_{Nr}} {w_{C1}} p_{C1})\)`.
- _Detection rate_ __upper limit__ `\(max(\tau_{r}) = -log(1- \frac {w_{Cr}} {w_{C1}} p_{C1})\)`.
- _Detection rate_ `\((\tau_r)\)` __decreases daily__ by `\(\times (1-\frac 1 7)\)`.


---
class: larger
# Wild boar model - fence, hunting &amp; military interv.
__Fence__
- Fence efficacy `\((\omega_{Fence})\)` multiplicative connectivity reduction.

__Hunting__
- `\(p_{HuntY} \sim Beta(330, 330)\)`  probability shot in 1 year.
- `\(\tau_H = -log((1 - p_{HuntY})^{12/(8 \times 365)})\)` daily hunting rate.
- `\(\tau_{Hhz} \sim Exp(\lambda = 10^{-7})\)`  additive hunting rate increase.

__Passive search__
- `\(\tau_P \sim LogNorm(lmean=0, lsd=3)\)`  detection rate.
- `\(\tau_{Phz} \sim Exp(\lambda = 10^{-7})\)`   additive detection rate increase.


---
class: larger
# Wild boar model - evolution

__Report 1__
- Active search global &amp; homogeneous
- Prediction via weighted average


__Report 2__
- Improved active search
- Added increased hunting rate parameter
- Some priors made less vague (aim: better mixing)
- Simplified weights for predictions


__Report 3__
- Returned to `\(\omega_{Fence} \sim Beta(1,1)\)`




---
class: larger
# Wild boar model - fixed values

.tiny[
| Notation          | Parameter                                         | Value                                               |
|-------------------|---------------------------------------------------|-----------------------------------------------------|
| `\(\omega\)`          | Preference for forest                             | 0.8                                                 |
| `\(\tau_I\)`          | ASF incubation rate                               | 1/7                                                 |
| `\(\tau_C\)`          | ASF induced mortality rate                        | 1/7                                                 |
| `\(p_\text{Rec}\)`    | Probability to recover                            | 1/20                                                |
| `\(\tau_\text{Rec}\)` | Recovery rate                                     | `\(\tau_C \frac{ p_\text{Rec}}{ (1-p_\text{Rec})}\)`    |
| `\(\tau_\text{Rot}\)` | Carcass decontamination rate                      | 1/90                                                |
| `\(\tau_H\)`          | Daily hunting rate                                | `\(-log((1-p_\text{HuntY})^{\frac{12}{8\times 365}})\)` |
| `\(p_\text{Test}\)`   | Probability test hunted animal                    | 1/5                                                 |
| `\(\beta_I\)`         | Transmission rate (infectious)                    | `\(\beta  p_\text{AttractI}\)`                          |
| `\(\beta_C\)`         | Transmission rate (carcass)                       | `\(\beta  (1-p_\text{AttractI})\)`                      |
| `\(\tau_A\)`          | Carcass detection rate (active search, report 1)  | `\(\tau_\text{Det}  p_\text{Active}\)`                  |
| `\(\tau_P\)`          | Carcass detection rate (passive search, report 1) | `\(\tau_\text{Det}  (1-p_\text{Active})\)`              |
]


---
class: larger
# Wild boar model - priors

.tiny[
| Notation              | Parameter                                       | Prior report 3                    | Diff. report 2 | Diff. report 1           |
|-----------------------|-------------------------------------------------|-----------------------------------|----------------|--------------------------|
| `\(t_\text{Intro}\)`      | ASF introduction time                           | -100 `\(\times\)` Beta(7,7)           |                | Unif(-90,0)              |
| `\(x_\text{Intro}\)`      | ASF introduction (easting)                      | Unif(extent of island)            |                |                          |
| `\(y_\text{Intro}\)`      | ASF introduction (northing)                     | Unif(extent of island)            |                |                          |
| `\(\beta\)`               | Transmission rate                               | Exp( `\(\lambda=0.1\)` )              |                | Exp( `\(\lambda=10^{-7}\)` ) |
| `\(p_\text{AttractI}\)`   | Attractivity of living IWB relative to carcass  | Beta(2,2)                         |                | Beta(1,1)                |
| `\(\tau_\text{P}\)`       | Passive search detection rate                   | LogNorm(lmean=0, lsd=3)           |                | N.A.                     |
| `\(\tau_\text{A}\)`       | Baseline active search detection rate           | Exp( `\(\lambda=10^{-7}\)` )          |                | N.A.                     |
| `\(\tau_\text{Phz}\)`     | Augmentation of `\(\tau_\text{P}\)` in hunting zone | Exp( `\(\lambda=10^{-7}\)` )          |                | N.A.                     |
| `\(\tau_\text{Hhz}\)`     | Augmentation of `\(\tau_\text{H}\)` in hunting zone | Exp( `\(\lambda=10^{-7}\)` )          |                | N.A.                     |
| `\(p_\text{HuntY}\)`      | Prob. hunted in 1 year                          | Beta(330,330)                     |                |                          |
| `\(\omega_\text{Fence}\)` | Efficacy of fence                               | Beta(1,1)                         | Beta(2,2)      | N.A.                     |
| `\(p_\text{Home}\)`       | Connectivity (prob. WB in home pixel)           | Fixed at `\(\text{logit}^{-1}\)`(9.8) | Beta(10,10)    | Beta(1,1)                |
| `\(\tau_\text{Det}\)`     | Global carcass detection rate                   | N.A.                              | N.A.           | Exp( `\(\lambda=10^{-7}\)` ) |
| `\(p_\text{Active}\)`     | Active search proportion of carcasses           | N.A.                              | N.A.           | Beta(1,1)                |
]


???
Report 1
- `\(t_{Intro} \sim Unif(-90, 0)\)` -- posterior too close to 0
- `\(p_{Home} \sim Beta(1,1)\)` -- huge correlation with `\(\beta\)`
- `\(\tau_{Det} \sim Exp(\lambda=10^{7})\)` -- passive search degeneracy
- `\(p_{Active} \sim Beta(1,1)\)` -- passive search degeneracy

Report 2
- `\(p_{Home} \sim Beta(10,10)\)` -- still huge correlation with `\(\beta\)`
- `\(\omega_{Fence} \sim Beta(2,2)\)` -- potential bias away from 1.




---
class: larger
# Wild boar model - likelihood

- Observed cases __ `\(y_{i,j,k}\)` __,
of __type (_i_)__, __time interval (_j_)__ and __pixel (_k_)__,
assumed __independant Poisson__ random variables.

- __Rate__ set to __expected value__ (Poisson-Gamma model) from __ `\(n_{Sim}\)` simulations__

`$$\tilde\lambda_{i,j,k}=\frac{1+\sum_1^n \tilde y_{i,j,k}}{1+n_{Sim}}$$`

- Case data aggregated in 10 day time intervals



---
class: larger
# Wild boar model - estimation

MCMC with __adaptive block Metropolis-Hastings__ sampler.

__Report 1__
- `\(n_{sim}=500\)` &amp; 20000 iterations
- Poor mixing (converged to target, but not finished adapting)

__Report 2__
- `\(n_{sim}=500\)` &amp; 5 `\(\times\)` 4000 iterations

__Report 3__
- `\(n_{sim}=1000\)` &amp; 5 `\(\times\)` 15000 iterations
- Fixed `\(p_{Home}=logit^{-1}(9.8)\)` (correlation with `\(\beta \approx 0.99\)` )


---
class: larger
# Wild boar model - prediction

* Predictions were made for __infectious wild boar__, i.e. compartment __I__.

* Saved the __mean__ and __standard deviation__ of __I__ from __5000__ (report 1) and, __10000__ (reports 2 &amp; 3) simulations, for either

		i) each pixel, at the end of the prediction interval, or,

		ii) globally, for each time-step of the prediction interval.

* Report 1 only: used likelihood of individual simulations to weight means and standard deviations.




---
class: larger
# Wild boar model - observed vs. predicted

.pull-left[
__Obs. cummul. incidence__
![](presentation_astre_files/figure-html/aggCumIWB_day110-1.png)&lt;!-- --&gt;
- Aggregated cases D0-D110
]


.pull-right[
__Pred. cummul. incidence__
![](presentation_astre_files/figure-html/aggCumIWB_predRep2-1.png)&lt;!-- --&gt;
- Fitted to data D0-80
- Mean of 10000 simulations

]

---
class: larger
# Wild boar model - projections (I)

.pull-left[
__Report 2__
![](presentation_astre_files/figure-html/IWB-scenarios-report2-1.png)&lt;!-- --&gt;
]

.pull-right[
__Report 3__
![](presentation_astre_files/figure-html/IWB-scenarios-report3-1.png)&lt;!-- --&gt;
]


- Increased hunting - strong effect
- Fencing           - weak effect (due to location)
- 2km active search - zero effect


---
class: larger
# Wild boar model - projections (II)

.pull-left[
![](presentation_astre_files/figure-html/stochasticTemporal-1.png)&lt;!-- --&gt;
]
.pull-right[
![](presentation_astre_files/figure-html/simStDevI-model12-1.png)&lt;!-- --&gt;
]




---
class: inverse, middle, center
# Conclusions

---
# Farm infection model

.pull-left[
&lt;svg viewBox="0 0 448 512" style="height:1em;position:relative;display:inline-block;top:.1em;fill:green;" xmlns="http://www.w3.org/2000/svg"&gt;  &lt;path d="M400 480H48c-26.51 0-48-21.49-48-48V80c0-26.51 21.49-48 48-48h352c26.51 0 48 21.49 48 48v352c0 26.51-21.49 48-48 48zm-204.686-98.059l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.248-16.379-6.249-22.628 0L184 302.745l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.25 16.379 6.25 22.628.001z"&gt;&lt;/path&gt;&lt;/svg&gt; __Pros__
- __Simple__ &amp; __robust__

- Integrated __three transmission pathways__

- __Predictions__ seem good
]

.pull-right[
&lt;svg viewBox="0 0 448 512" style="height:1em;position:relative;display:inline-block;top:.1em;fill:darkred;" xmlns="http://www.w3.org/2000/svg"&gt;  &lt;path d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zM92 296c-6.6 0-12-5.4-12-12v-56c0-6.6 5.4-12 12-12h264c6.6 0 12 5.4 12 12v56c0 6.6-5.4 12-12 12H92z"&gt;&lt;/path&gt;&lt;/svg&gt; __Cons__
- Predictions __static not dynamic__

- Not linked to __wild boar model__
]



---
# Wild boar model

.pull-left[
&lt;svg viewBox="0 0 448 512" style="height:1em;position:relative;display:inline-block;top:.1em;fill:green;" xmlns="http://www.w3.org/2000/svg"&gt;  &lt;path d="M400 480H48c-26.51 0-48-21.49-48-48V80c0-26.51 21.49-48 48-48h352c26.51 0 48 21.49 48 48v352c0 26.51-21.49 48-48 48zm-204.686-98.059l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.248-16.379-6.249-22.628 0L184 302.745l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.25 16.379 6.25 22.628.001z"&gt;&lt;/path&gt;&lt;/svg&gt; __Pros__
- Likelihood mathematically __simple__

- __Adaptive MCMC__ more efficient than ABC

- __Bayesian__ inference

- __Predictions__ consistently good
]

.pull-right[
&lt;svg viewBox="0 0 448 512" style="height:1em;position:relative;display:inline-block;top:.1em;fill:darkred;" xmlns="http://www.w3.org/2000/svg"&gt;  &lt;path d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zM92 296c-6.6 0-12-5.4-12-12v-56c0-6.6 5.4-12 12-12h264c6.6 0 12 5.4 12 12v56c0 6.6-5.4 12-12 12H92z"&gt;&lt;/path&gt;&lt;/svg&gt; __Cons__
- __5km resolution__ very coarse for active search

- Computationally __slow__

- __Strong correlations__ made MCMC challenging

- Not linked to __pig farm model__
]


---
# Possible improvements

.smallish[
- __Stronger links__ between the __two models__.

- Improved  __temporal dimension__ of __farm model__.

- Upgrade __probabilities of future farm to farm shipments__ from empirical rates to something better. Network model?

- __Remove culled periods__ from the __number of days at risk__ during observation &amp; prediction periods.

- Relax __independence assumption__ for farm contamination pathways.

- Replace  simplifying expected values with __full probabilistic model__.

- __Sub-pixel__ active search.
]

???

The assumptions of homogeneity and independence of risk from wild boar in the observation and prediction periods are probably the most questionable of our approximations, as the risk of transmission certainly evolves in time. We only updated our estimates 3 times at each submission stage.

We tried to use the estimated density of infectious wild boar from the wild boar model simulations as a measure of exposure. But it has shown a much inferior predictive power than the cumulative wild boar case density.

It is possible that the cumulative density accounts also for carcasses and virus left over the environment and potentially introduced into the farms via fomites.

Perhaps the fomites and wild boar pathways should be merged using the wild boar case and farm densities as exposure variables and the outdoor indicator as a fixed factor.




---
# Thanks!

All our code, reports, exploratory analyses, mistakes and abandoned tests are openly available at

https://forgemia.inra.fr/umr-astre/asf-challenge


&lt;p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"&gt;&lt;a property="dct:title" rel="cc:attributionURL" href="https://forgemia.inra.fr/umr-astre/asf-challenge"&gt;ASF Challenge modelling&lt;/a&gt; by &lt;a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://umr-astre.cirad.fr/"&gt;Astre Team&lt;/a&gt; is licensed under &lt;a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;"&gt;CC BY-SA 4.0&lt;img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"&gt;&lt;img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"&gt;&lt;img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"&gt;&lt;/a&gt;&lt;/p&gt;

.small[
Icons credits:
- http://icons8.com
- http://www.streamlineicons.com
- https://fontawesome.com/
]



&lt;!----------------------------------------------------------------------------------------------------------&gt;
&lt;!-- Limitations of math in xarigan																		  --&gt;
&lt;!-- 																									  --&gt;
&lt;!-- The source code of a LaTeX math expression must be in one line, unless it is inside a pair of double --&gt;
&lt;!-- dollar signs, in which case the starting $$ must appear in the very beginning of a line, followed	  --&gt;
&lt;!-- immediately by a non-space character, and the ending $$ must be at the end of a line, led by a		  --&gt;
&lt;!-- non-space character;																				  --&gt;
&lt;!-- 																									  --&gt;
&lt;!-- There should not be spaces after the opening $ or before the closing $.							  --&gt;
&lt;!-- 																									  --&gt;
&lt;!-- Math does not work on the title slide (see #61 for a workaround).									  --&gt;
&lt;!----------------------------------------------------------------------------------------------------------&gt;
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script src="macros.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false,
"ratio": "16:9"
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
