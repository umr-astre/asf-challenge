## Objective: make bins more consistent and sensible.
library(nimble)

nTimeChunks   = 5
stepsPerChunk = 3

dtDynamics   = 1.0 # Resolution of discretisation of dynamic process
tStartObs    = 0
tStop        = nTimeChunks * stepsPerChunk * dtDynamics # tStop always a multiple of stepsPerChunk 
(tIntro      = runif(1, -90, 0))
nTimeChunks1 = nTimeChunks + 1
dtDynamics0  = ceiling(tIntro) - tIntro # For very first step 

breaks = nimInteger(value=0, length=nTimeChunks1)
for (br in 1:nTimeChunks)
  breaks[br+1] = br * stepsPerChunk


for (t in -11:tStop) {
  if (t>=0) {
    iChunk <- min(which(t<=breaks))
    nimPrint(t, " ", iChunk)
  }
}



dim(wildBoarArrayAll)
dim(cWildBoarModelSub$obsY)
dim(cWildBoarModelAll$obsY)


simulate(rWildBoarModelSub, detNodes)
