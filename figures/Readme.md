## Manual processing of svg files

Since I can't put the legend where I want using tmap's interface, I exported to svg with an outer legend and then manually edit it with Inkscape to put the legend over the first facet.
